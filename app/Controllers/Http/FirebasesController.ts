import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import FirebaseService from 'App/Services/FIrebaseService'

export default class FirebasesController {
  public async addAdminToken({ request, response }: HttpContextContract) {
    const token = request.input('token')
    await FirebaseService.setAdminToken(token)
    return response.ok({ message: 'Token added' })
  }
}
