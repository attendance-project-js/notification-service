import Redis from '@ioc:Adonis/Addons/Redis'
import * as firebaseAdmin from 'firebase-admin'
import { MessagePayload } from 'App/Models/MessagePayload'
import { initFirebase } from '../../start/firebase'

const ADMIN_TOKENS = 'admin_tokens'
export default class FirebaseService {
  public static async sendAdminNotification(payload: MessagePayload) {
    await initFirebase();
    await Redis.lrange(ADMIN_TOKENS, 0, -1).then((tokens) => {
      tokens.map(async (token) => {
        await this.sendToDevice(token, payload.title, payload.body)
      })
    })
  }

  public static async sendToDevice(token: string, title: string, body: string) {
    await firebaseAdmin.messaging().sendToDevice(token, {
      notification: {
        title: title,
        body: body,
      },
    })
  }

  public static async setAdminToken(token: string) {
    // await Redis.hset(ADMIN_TOKENS, userId, token, 'EX', 5)
    await Redis.lpush(ADMIN_TOKENS, token, 'EX', 5)
  }
}
