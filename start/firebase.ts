const serviceAccount = require('../firebase.config.json')
import * as firebaseAdmin from 'firebase-admin'

export async function initFirebase() {
  if(!firebaseAdmin.apps.length)
    firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.cert(serviceAccount),
    })
}

module.exports = { initFirebase }
