import Rabbit from '@ioc:Adonis/Addons/Rabbit'
import { QUEUE_ADMIN_NOTIFICATION } from 'Config/rabbit'
// @ts-ignore
import { firebaseAdmin, initFirebase } from './firebase'
import * as console from 'console'
import { MessagePayload } from 'App/Models/MessagePayload'
import FirebaseService from 'App/Services/FIrebaseService'

async function listen() {
  console.log(`Listening to ${QUEUE_ADMIN_NOTIFICATION} queue`)
  
  await Rabbit.assertQueue(QUEUE_ADMIN_NOTIFICATION)
  await Rabbit.consumeFrom(QUEUE_ADMIN_NOTIFICATION, async (message) => {
    console.log(`Received message from queue ${QUEUE_ADMIN_NOTIFICATION}`)
    const payload = JSON.parse(message.content.toString()) as MessagePayload
    console.log("payload", payload)
    await FirebaseService.sendAdminNotification(payload)
    message.ack()
  })
}

listen()
